package popschool.cinema_rest_spring_hateos_client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import uk.co.blackpepper.bowman.InlineAssociationDeserializer;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Data
@RemoteResource("/pays")
public class Pays {

    private URI id;

    private String nom;

    private List<Acteur> acteurs = new ArrayList<>();

    //permet de regler le problème des many to one et one to many du coté client
    @JsonDeserialize(contentUsing = InlineAssociationDeserializer.class)
    //@LinkedResource
    public List<Acteur> getActeurs(){ return acteurs;}

    @ResourceId
    public URI getId(){
        return id;
    }

    public Pays(String nom) {
        this.nom = nom;
    }

    public Pays() {
    }

    @Override
    public String toString() {
        return "Pays{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", acteurs=" + acteurs +
                '}';
    }
}

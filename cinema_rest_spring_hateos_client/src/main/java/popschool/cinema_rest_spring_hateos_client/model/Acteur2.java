package popschool.cinema_rest_spring_hateos_client.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;

import java.util.Date;

@Data
@RemoteResource("/acteurs")
public class Acteur2 {


    private String nom;
    private String prenom;
    private Date naissance;



}

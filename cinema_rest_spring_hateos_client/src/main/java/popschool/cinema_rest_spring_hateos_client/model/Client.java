package popschool.cinema_rest_spring_hateos_client.model;

import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class Client {

    private Long nclient;
    private String nom;
    private String prenom;
    private String adresse;
    private Integer anciennete;
    private List<Emprunt> emprunt;

    public Client() {
    }

    public Client(String nom, String prenom, String adresse, Integer anciennete) {
        this.nom=nom.toUpperCase();
        this.prenom=prenom.toUpperCase();
        this.adresse=adresse.toUpperCase();
        this.anciennete=anciennete;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nclient=" + nclient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", anciennete=" + anciennete +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return nom.equals(client.nom) &&
                prenom.equals(client.prenom) &&
                adresse.equals(client.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom, adresse);
    }
}

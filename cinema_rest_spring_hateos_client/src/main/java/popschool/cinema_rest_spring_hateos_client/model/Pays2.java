package popschool.cinema_rest_spring_hateos_client.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import uk.co.blackpepper.bowman.InlineAssociationDeserializer;
import uk.co.blackpepper.bowman.annotation.RemoteResource;

import java.util.ArrayList;
import java.util.List;

@Data
@RemoteResource("/pays")
public class Pays2 {

    private String nom;


}

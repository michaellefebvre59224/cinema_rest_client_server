package popschool.cinema_rest_spring_hateos_client.model;

import net.minidev.json.JSONUtil;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences;
import org.springframework.http.MediaType;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;
import java.util.Collection;
import java.util.Date;

public class CinemaTest {

    public static void main(String[] args) {

        /*String URL_LOCAL = "http://localhost:8081/";

        ClientFactory clientFactory = Configuration.builder().build().buildClientFactory();*/

        /*Client<Pays> client = clientFactory.create(Pays.class);
        Iterable<Pays> pays = client.getAll(URI.create("http://localhost:8081/pays"));
        System.out.println("////////////////GETALL//////////////////////////////////");
        System.out.println(pays);

        ClientFactory clientFactory2 = Configuration.build().buildClientFactory();

        Client<Pays> client2 = clientFactory2.create(Pays.class);
        Pays pays2 = client2.get(URI.create("http://localhost:8081/pays/1"));

        System.out.println("////////////////GET//////////////////////////////////");
        System.out.println(pays2);

        Traverson client3 = new Traverson(URI.create("http://localhost:8081/"), MediaTypes.HAL_JSON);
        CollectionModel<EntityModel<Acteur2>> acteurs = client3//
            .follow("acteurs")//
            .toObject(new TypeReferences.CollectionModelType<EntityModel<Acteur2>>() {
        });
        System.out.println("////////////////TRAVERSON//////////////////////////////////");
        acteurs.forEach(r -> {
            System.out.println(r.getContent());
        });*/


        //post creation d'un pays
        /*Client<Pays> clientPays = clientFactory.create(Pays.class);
        URI id = clientPays.post(new Pays("Finlande"));*/


      /* Client<Acteur> clientActeur = clientFactory.create(Acteur.class);
        URI id2 = clientActeur.post(new Acteur("ellington", "Duke"));*/

        String URL_LOCAL = "http://localhost:8081/";
        ClientFactory clientFactory = Configuration.builder().setBaseUri(URL_LOCAL).build().buildClientFactory();
        Client<Pays> clientPays = clientFactory.create(Pays.class);
        Client<Acteur> clientActeur = clientFactory.create(Acteur.class);

        Pays nationalite = new Pays();
        nationalite.setNom("Test");
        URI id = clientPays.post(nationalite);
        nationalite.setId(id);
        Acteur acteur = new Acteur();
        acteur.setPays(nationalite);
        acteur.setNom("newTest");
        acteur.setPrenom("Getit");
        Date date = new java.sql.Date(1986,02,24);
        acteur.setNaissance(date);
        URI uri2 = clientActeur.post(acteur);



    }


}

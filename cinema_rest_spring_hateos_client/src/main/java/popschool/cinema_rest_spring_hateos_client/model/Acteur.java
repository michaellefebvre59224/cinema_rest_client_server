package popschool.cinema_rest_spring_hateos_client.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@RemoteResource("/acteurs")
public class Acteur {


    private URI id;

    private String nom;
    private String prenom;
    private Date naissance;
    private Pays pays;
    private Integer nbrefilms;
    @LinkedResource
    public Pays getPays() {
        return pays;
    }

    @ResourceId
    public URI getId(){
        return id;
    }

    public Acteur(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Acteur() {
    }
}

package popschool.cinema_rest_spring_hateos_client.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data

public class Genre {

    private Long ngenre;
    private String nature;
    private List<Film> films = new ArrayList<>();

    @Override
    public String toString() {
        return
                "{ nature='" + nature + '\'' +
                '}';
    }
}

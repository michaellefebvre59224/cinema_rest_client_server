package popschool.cinema_rest_spring_hateaos_server.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="emprunt")
public class Emprunt {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long nemprunt;

    @ManyToOne
    @JoinColumn(name = "nclient")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "nfilm")
    private Film film;

    private String retour;
    private Date dateemprunt;

    public Emprunt() {
    }

    public Emprunt(Client client, Film film, String retour, Date dateemprunt){
        this.client=client;
        this.film=film;
        this.retour=retour;
        this.dateemprunt=dateemprunt;
    }

    @Override
    public String toString() {
        return "Emprunt{" +
                "nemprunt=" + nemprunt +
                ", client=" + client.getNom() +
                ", film=" + film.getTitre() +
                ", retour='" + retour + '\'' +
                ", dateemprunt=" + dateemprunt +
                '}';
    }
}

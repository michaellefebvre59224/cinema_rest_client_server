package popschool.cinema_rest_spring_hateaos_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name="client")
public class Client {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long nclient;

    private String nom;
    private String prenom;
    private String adresse;
    private Integer anciennete;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Emprunt> emprunt;

    public Client() {
    }

    public Client(String nom, String prenom, String adresse, Integer anciennete) {
        this.nom=nom.toUpperCase();
        this.prenom=prenom.toUpperCase();
        this.adresse=adresse.toUpperCase();
        this.anciennete=anciennete;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nclient=" + nclient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", anciennete=" + anciennete +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return nom.equals(client.nom) &&
                prenom.equals(client.prenom) &&
                adresse.equals(client.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom, adresse);
    }
}

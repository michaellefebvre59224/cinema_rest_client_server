package popschool.cinema_rest_spring_hateaos_server.dao;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.cinema_rest_spring_hateaos_server.model.Client;


import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "clients", path = "clients")
public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {

    //Trouver un client en fonction du nom
    Optional<Client> findByNomLike(String nom);

    //Trouver tous les client ordonnés par nom
    List<Client> findAllByOrderByNom();



}

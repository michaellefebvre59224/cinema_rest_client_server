package popschool.cinema_rest_spring_hateaos_server.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.cinema_rest_spring_hateaos_server.model.Acteur;
import popschool.cinema_rest_spring_hateaos_server.model.FilmDto;


import java.util.Date;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "acteurs", path = "acteurs")
public interface ActeurRepository  extends PagingAndSortingRepository<Acteur , Long> {

    Optional<Acteur> findByNomLike(String nom);

    List<Acteur> findByPrenomLike(String prenom);

    List<Acteur> findByNaissanceAfter(Date date);

    List<Acteur> findByNaissanceBefore(Date date);


    List<Acteur> findByNbrefilmsGreaterThan(int nbrefilms);

    List<Acteur> findByNomContaining(String nom);

    List<Acteur> findByPays_Nom(String pays);

    //Methode dto
    @Query(value = "select new popschool.cinema_rest_spring_hateaos_server.model.FilmDto(a.pays.nom, count(a)) " +
            " from Acteur a " +
            " group by a.pays.nom")
    List<FilmDto> findNbreActeurByPays();

}

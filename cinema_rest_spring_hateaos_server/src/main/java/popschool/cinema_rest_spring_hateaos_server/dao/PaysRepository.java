package popschool.cinema_rest_spring_hateaos_server.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.cinema_rest_spring_hateaos_server.model.Genre;
import popschool.cinema_rest_spring_hateaos_server.model.Pays;

@RepositoryRestResource(collectionResourceRel = "pays", path = "pays")
public interface PaysRepository extends PagingAndSortingRepository<Pays, Long> {



}

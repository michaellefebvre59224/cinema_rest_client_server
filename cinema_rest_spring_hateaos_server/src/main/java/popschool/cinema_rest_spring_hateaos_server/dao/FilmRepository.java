package popschool.cinema_rest_spring_hateaos_server.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.cinema_rest_spring_hateaos_server.model.Client;
import popschool.cinema_rest_spring_hateaos_server.model.Film;


import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "films", path = "films")
public interface FilmRepository extends PagingAndSortingRepository<Film, Long> {

    List<Film> findByGenre_Nature(String drame);

    List<Film> findByActeur_Nom(String depardieu);

    List<Film> findByPays_Nom(String france);

    Optional<Film> findByTitreLike(String titre);

    List<Film> findAllByOrderByTitre();

    @Query(value = "select f from Film f " +
            " where not exists(select e from Emprunt e where e.film = f and e.retour = 'NON') " +
            " order by f.titre")
    List<Film> findAllFilmEmpruntable();

    //methode Tuple
    @Query(value = "Select f.genre.nature as nature, count(f.genre.nature) as nombre" +
            " from Film f " +
            " group by f.genre.nature")
    List<Tuple> findNbreFilmsByGenre();

    //nombre de film du genre
    @Query(value = "Select count(f.genre.nature) as nombre" +
            " from Film f " +
            " WHERE f.genre.nature = :genre " +
            " group by f.genre.nature ")
    int findNbreFilmsByOneGenre(String genre);

    //afficher les film empruntés par 1 client
    @Query(value = "select f from Film f " +
            " where exists (select e from Emprunt e where e.film = f and e.client = :client  and e.retour = 'NON') " +
            " order by f.titre")
    List<Film> findFilmEmprunteByClient(Client client);

    //afficher les info realisateur et acteur d'un film
    //methode Tuple
    @Query(value = "Select f.realisateur as realisateur, f.acteur.nom as acteurnom" +
            " from Film f " +
            " where f.titre = :titre")
    List<Tuple> findinfoRealisateurAndActeur(String titre);


}

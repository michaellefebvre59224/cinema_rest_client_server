package popschool.cinema_rest_spring_hateaos_server.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.cinema_rest_spring_hateaos_server.model.Emprunt;


import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "emprunts", path = "emprunts")
public interface EmpruntRepository extends PagingAndSortingRepository<Emprunt, Long> {


    Optional<Emprunt> findByClient_NomAndFilm_TitreAndRetour(String nom, String titre, String non);
    List<Emprunt> findByRetour(String retour);
}

package popschool.cinema_rest_spring_hateaos_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaRestSpringHateaosServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaRestSpringHateaosServerApplication.class, args);
    }

}
